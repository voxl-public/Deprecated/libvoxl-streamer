## Build
Host requires voxl-docker (https://gitlab.com/voxl-public/voxl-docker) with emulator image.

### On Host:
Clone and initialize repository:
<pre>
$ git clone https://gitlab.com/voxl-public/libvoxl-streamer.git
$ cd libvoxl-streamer
</pre>

Build library:
<pre>
$ voxl-docker -i voxl-emulator
$ ./clean.sh
$ ./build.sh
$ ./make_package.sh
$ exit
</pre>

Install on target:
<pre>
$ ./install_on_voxl.sh
</pre>

This installs the shared library and header files on the target as follows:
* /usr/lib/libvoxl-streamer.so
* /usr/include/ServerUtils.hpp
* /usr/include/VoxlStreamer.hpp

## Usage

### On Host
python py/voxl-viewer.py -i \<ip address of target\>
