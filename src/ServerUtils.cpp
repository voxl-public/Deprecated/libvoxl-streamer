/*******************************************************************************
 * Copyright 2019 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include "ServerUtils.hpp"

/**
 * 
 */
int IpServer::CreateSocket(const char *ip_addr, int port_num)
{
  _sock = socket(AF_INET, SOCK_STREAM, 0);
  if (_sock < 0)
  {
    printf("ERROR: Could not create socket\n");
    return -1;
  }

  printf("Created socket\n");

  memset((void *)&_serv_addr, 0, sizeof(_serv_addr));
  _serv_addr.sin_family = AF_INET;
  _serv_addr.sin_addr.s_addr = inet_addr(ip_addr);
  _serv_addr.sin_port = htons(port_num);
  _serv_addr_size = sizeof(_serv_addr);

  return 0;
}

/**
 * 
 */
int IpServer::BindSocket(void)
{
  int sockoptval = 1;
  if (setsockopt(_sock, SOL_SOCKET, SO_REUSEADDR, &sockoptval, sizeof(int)) < 0)
  {
    printf("ERROR: Error with setsockopt\n");
    return -1;
  }

  int res = bind(_sock, (struct sockaddr *)&_serv_addr, sizeof(_serv_addr));
  if (res < 0)
  {
    printf("ERROR: Could not bind address to socket\n");
    return -1;
  }
  printf("Bind successful\n");

  return 0;
}

/**
 * 
 */
int IpServer::CheckSocket(void)
{
  int sockoptval = 1;
  socklen_t len = sizeof(sockoptval);
  if (getsockopt(_data_sock, SOL_SOCKET, SO_ERROR, &sockoptval, &len) < 0)
  {
    printf("ERROR: Error with getsockopt\n");
    return -1;
  }

  if (sockoptval == 0)
  {
    return 0;
  }

  printf("ERROR: Socket connection error\n");
  return -1;
}

/**
 * 
 */
int IpServer::ConnectClient(void)
{
  int res = listen(_sock, 5);
  if (res < 0)
  {
    printf("ERROR: Socket listen failed\n");
    return -1;
  }
  printf("Listen successful, waiting for connection...\n");

  struct sockaddr_in client_addr;
  socklen_t client_addr_size = sizeof(client_addr);
  _data_sock = accept(_sock, (struct sockaddr *)&client_addr, &client_addr_size);
  if (_data_sock < 0)
  {
    printf("ERROR: Connection accept failed [%d]\n", errno);
    return -1;
  }
  FD_ZERO(&_master);
  FD_SET(_data_sock, &_master);
  printf("Connection established\n");

  return 0;
}

/**
 * 
 */
int IpServer::RecvMessage(void)
{
  struct timeval tv;
  tv.tv_sec = 0;
  tv.tv_usec = 1000;
  _readfds = _master;
  if (select(_data_sock + 1, &_readfds, NULL, NULL, &tv) < 0)
  {
    printf("ERROR: Error with select\n");
    return -1;
  }
  if (FD_ISSET(_data_sock, &_readfds))
  {
    uint8_t recv_buf;
    int recvlen = recv(_data_sock, &recv_buf, 1, 0);
    if (recvlen < 0)
    {
      recv_buf = 0;
      return -1;
    }
    return recv_buf;
  }

  return 0;
}

/**
 * 
 */
int IpServer::SendMessage(const uint8_t *msg, size_t msg_len,
                          int msg_id, uint16_t num_cols, uint16_t num_rows,
                          uint8_t *opts, uint32_t frame, uint64_t timestamp)
{
  MessageHeader *msg_hdr = new MessageHeader();
  msg_hdr->message_id = msg_id;
  msg_hdr->flag = 0;
  msg_hdr->frame_id = frame;
  msg_hdr->timestamp_ns = timestamp;
  msg_hdr->num_cols = num_cols;
  msg_hdr->num_rows = num_rows;
  memcpy(msg_hdr->opts, opts, 4 * sizeof(uint8_t));
  msg_hdr->checksum = 0; // placeholder for checksum (not implemented)

  uint8_t *packet = new uint8_t[24];
  memcpy(packet, &msg_hdr->message_id, 1);
  memcpy(packet + 1, &msg_hdr->flag, 1);
  memcpy(packet + 2, &msg_hdr->frame_id, 4);
  memcpy(packet + 6, &msg_hdr->timestamp_ns, 8);
  memcpy(packet + 14, &msg_hdr->num_cols, 2);
  memcpy(packet + 16, &msg_hdr->num_rows, 2);
  memcpy(packet + 18, &msg_hdr->opts, 4);
  memcpy(packet + 22, &msg_hdr->checksum, 2);

  // First send the header
  int res = send(_data_sock, packet, 24, 0);
  if (res < 0)
  {
    printf("ERROR: Error in sending image header\n");
    return -1;
  }

  // Now send the image data
  res = send(_data_sock, msg, msg_len, 0);
  if (res < 0)
  {
    printf("ERROR: Error in sending image\n");
    return -1;
  }

  delete[] packet;
  delete[] msg_hdr;

  return 0;
}

/**
 * 
 */
int IpServer::SendMessage(const uint16_t *msg, size_t msg_len,
                          int msg_id, uint16_t num_cols, uint16_t num_rows,
                          uint8_t *opts, uint32_t frame, uint64_t timestamp)
{
  int m = sizeof(uint16_t) / sizeof(uint8_t);

  MessageHeader *msg_hdr = new MessageHeader();
  msg_hdr->message_id = msg_id;
  msg_hdr->flag = 0;
  msg_hdr->frame_id = frame;
  msg_hdr->timestamp_ns = timestamp;
  msg_hdr->num_cols = num_cols;
  msg_hdr->num_rows = num_rows;
  memcpy(msg_hdr->opts, opts, 4 * sizeof(uint8_t));
  msg_hdr->checksum = 0;

  uint8_t *packet = new uint8_t[24];
  memcpy(packet, &msg_hdr->message_id, 1);
  memcpy(packet + 1, &msg_hdr->flag, 1);
  memcpy(packet + 2, &msg_hdr->frame_id, 4);
  memcpy(packet + 6, &msg_hdr->timestamp_ns, 8);
  memcpy(packet + 14, &msg_hdr->num_cols, 2);
  memcpy(packet + 16, &msg_hdr->num_rows, 2);
  memcpy(packet + 18, &msg_hdr->opts, 4);
  memcpy(packet + 22, &msg_hdr->checksum, 2);

  int n = num_cols * num_rows;
  for (int i = 0; i < m; ++i)
  {
    // First send the header
    int res = send(_data_sock, packet, 24, 0);
    if (res < 0)
    {
      printf("ERROR: Error in sending image header\n");
      return -1;
    }

    // Now send the image data
    res = send(_data_sock, msg + (i * (n / m)), msg_len / m, 0);
    if (res < 0)
    {
      printf("ERROR: Error in sending image\n");
      return -1;
    }
  }

  delete[] packet;
  delete[] msg_hdr;

  return 0;
}

/**
 * 
 */
int IpServer::SendMessage(const float *msg, size_t msg_len,
                          int msg_id, uint16_t num_cols, uint16_t num_rows,
                          uint8_t *opts, uint32_t frame, uint64_t timestamp)
{
  int m = sizeof(float) / sizeof(uint8_t);

  MessageHeader *msg_hdr = new MessageHeader();
  msg_hdr->message_id = msg_id;
  msg_hdr->flag = 0;
  msg_hdr->frame_id = frame;
  msg_hdr->timestamp_ns = timestamp;
  msg_hdr->num_cols = num_cols;
  msg_hdr->num_rows = num_rows;
  memcpy(msg_hdr->opts, opts, 4 * sizeof(uint8_t));
  msg_hdr->checksum = 0;

  uint8_t *packet = new uint8_t[24];
  memcpy(packet, &msg_hdr->message_id, 1);
  memcpy(packet + 1, &msg_hdr->flag, 1);
  memcpy(packet + 2, &msg_hdr->frame_id, 4);
  memcpy(packet + 6, &msg_hdr->timestamp_ns, 8);
  memcpy(packet + 14, &msg_hdr->num_cols, 2);
  memcpy(packet + 16, &msg_hdr->num_rows, 2);
  memcpy(packet + 18, &msg_hdr->opts, 4);
  memcpy(packet + 22, &msg_hdr->checksum, 2);

  int n = num_cols * num_rows;
  for (int i = 0; i < m; ++i)
  {
    // First send the header
    int res = send(_data_sock, packet, 24, 0);
    if (res < 0)
    {
      printf("ERROR: Error in sending image header (%d)\n", res);
      return -1;
    }

    // Now send the image data
    res = send(_data_sock, msg + (i * (n / m)), msg_len / m, 0);
    if (res < 0)
    {
      printf("ERROR: Error in sending image (%d)\n", res);
      return -1;
    }
  }

  delete[] packet;
  delete[] msg_hdr;

  return 0;
}

/**
 * Closes sockets
 */
IpServer::~IpServer(void)
{
  close(_sock);
  close(_data_sock);
  printf("Closing sockets\n");
}
