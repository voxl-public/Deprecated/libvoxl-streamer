/*******************************************************************************
 * Copyright 2019 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef VOXL_STREAMER_HPP
#define VOXL_STREAMER_HPP

#include <cstdbool>
#include <cstring>
#include <deque>
#include <fstream>

#include "ServerUtils.hpp"

enum MsgId
{
  MONOCHROME_IMG = 2,
  MONOCHROME_STEREO_IMG_L = 20,
  MONOCHROME_STEREO_IMG_R = 21,
  VALUE_PAIR = 200
};

class VoxlStreamer
{
public:
  /**
   * Constructor
   */
  VoxlStreamer(bool verbose) : _verbose(verbose)
  {
    _initialized = false;
    _server_ptr = NULL;
  }

  /**
   * Initialize the VoxlStreamer from IP and Port
   */
  int Initialize(char*, int);

  /**
   * Initialize the VoxlStreamer from IpServer
   */
  int Initialize(IpServer*);

  /**
   * Check the TCP/IP connection
   */
  int CheckConnection();

  /**
   * Reset
   */
  void Reset();

  /**
   * Get the pointer to the IpServer
   */
  inline IpServer* GetServerPtr()
  {
    return _server_ptr;
  }

  /**
   * Receive a keyboard command from the connected computer
   */
  int ReceiveCommand();

  /**
   * Send an image over TCP/IP connection
   */
  void SendImage(uint8_t*, const uint16_t, const uint16_t,
                 uint8_t[4], const uint32_t, const uint64_t);

  /**
   * Send stereo images over TCP/IP connection
   */
  void SendStereoImage(uint8_t*, uint8_t*,
                       const uint16_t, const uint16_t,
                       uint8_t[4], const uint32_t, const uint64_t);

  /**
   * Send a (label, value) pair over TCP/IP connection
   */
  void SendValue(char*, double, uint8_t,
                 uint8_t[4], const uint32_t, const uint64_t);

private:
  /**
   * Class instance of TCP/IP socket connection
   */
  IpServer* _server_ptr;

  bool _initialized;

  const bool _verbose;
};

#endif // VOXL_STREAMER_HPP
